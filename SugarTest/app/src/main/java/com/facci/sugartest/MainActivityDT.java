package com.facci.sugartest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.facci.sugartest.entity.Semestre;

import java.util.Iterator;
import java.util.List;

public class MainActivityDT extends AppCompatActivity {

    EditText txtID;
    EditText txtNombre;
    EditText txtApellido;
    EditText txtSemestre;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_dt);
    }

    public void ingresarClick(View v){

        txtNombre = (EditText) findViewById(R.id.txtNombre);
        txtApellido = (EditText) findViewById(R.id.txtApellido);
        txtSemestre = (EditText) findViewById(R.id.txtSemestre);

        Semestre objetoSemestre = new Semestre(txtNombre.getText().toString(),txtApellido.getText().toString(),Integer.parseInt(txtSemestre.getText().toString()));
        long ingresar = objetoSemestre.save();

    }

    public void modificarClick(View v){

        txtID = (EditText) findViewById(R.id.txtID);
        txtNombre = (EditText) findViewById(R.id.txtNombre);
        txtApellido = (EditText) findViewById(R.id.txtApellido);
        txtSemestre = (EditText) findViewById(R.id.txtSemestre);

        Semestre objetoSemestre = Semestre.findById(Semestre.class,Integer.parseInt(txtID.getText().toString()));

        objetoSemestre.nombre = txtNombre.getText().toString();
        objetoSemestre.apellido = txtApellido.getText().toString();
        objetoSemestre.semestre = Integer.parseInt(txtSemestre.getText().toString());

        objetoSemestre.save();

    }


    public void eliminarClick(View v){

        txtID = (EditText) findViewById(R.id.txtID);
        Semestre objetoSemestre = Semestre.findById(Semestre.class,Integer.parseInt(txtID.getText().toString()));
        objetoSemestre.delete();

    }

    public void verTodosClick(View v){
        Iterator<Semestre> listaSemestres = Semestre.findAll(Semestre.class);

        while(listaSemestres.hasNext()){
            Semestre objetoSemestre = listaSemestres.next();
            Toast.makeText(MainActivityDT.this,objetoSemestre.nombre+ " "+objetoSemestre.apellido, Toast.LENGTH_SHORT).show();
        }

    }
}
